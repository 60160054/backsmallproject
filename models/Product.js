const mongoose = require('mongoose')
const Schema = mongoose.Schema
const productSchema = new Schema({
  name: String,
  price: Number,
  cost: Number
})

const Product = mongoose.model('Product', productSchema)

Product.find(function (err, products) {
  if (err) return console.error(err)
  console.log(products)
})

module.exports = mongoose.model('Product', productSchema)
